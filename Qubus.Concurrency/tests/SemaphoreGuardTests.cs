using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace Qubus.Concurrency.Tests
{
    public class SemaphoreGuardTests
    {
        [Test]
        public async Task ABinarySemaphoreIsAcquired_CountDropsToZero()
        {
            var semaphore = new SemaphoreSlim(1, 1);

            using var guard = await SemaphoreGuard.Acquire(semaphore, CancellationToken.None);
            
            Assert.AreEqual(0, semaphore.CurrentCount);
        }
        
        [Test]
        public async Task TestDispose_SemaphoreHasBeenAcquired_SemaphoreIsReleased()
        {
            var semaphore = new SemaphoreSlim(1, 1);

            {
                using var guard = await SemaphoreGuard.Acquire(semaphore, CancellationToken.None);
            }

            Assert.AreEqual(1, semaphore.CurrentCount);
        }
    }
}