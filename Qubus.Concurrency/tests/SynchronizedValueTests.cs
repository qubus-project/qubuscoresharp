using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace Qubus.Concurrency.Tests
{
    public class SynchronizedValueTests
    {
        [Test]
        public async Task ApplyTheDelegate_TheValueHasBeenRecorded()
        {
            var value = new SynchronizedValue<int>(42);

            int recordedValue = 0;
            
            await value.Apply(value => recordedValue = value, CancellationToken.None);
            
            Assert.AreEqual(42, recordedValue);
        }
        
        [Test]
        public async Task AcquireTheValue_TheValueCanBeAccessed()
        {
            var value = new SynchronizedValue<int>(42);

            using var guard = await value.Acquire(CancellationToken.None);
            
            Assert.AreEqual(guard.Value, 42);

            guard.Value = 40;
            
            Assert.AreEqual(guard.Value, 40);
        }
    }
}