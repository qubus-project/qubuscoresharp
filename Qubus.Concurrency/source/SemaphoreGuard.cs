using System;
using System.Threading;
using System.Threading.Tasks;

namespace Qubus.Concurrency
{
    /// <summary>
    ///     A resource guard which will safely acquire a semaphore and releases it upon being disposed.
    /// </summary>
    /// <remarks>
    ///     This class is thread-safe.
    /// </remarks>
    public readonly struct SemaphoreGuard : IDisposable
    {
        /// <summary>
        ///     Acquires the semaphore.
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         The semaphore is acquired as soon as the task completes.
        ///     </para>
        ///
        ///     <para>
        ///         The semaphore will not be released if it could not be acquired due to cancellation.
        ///     </para>
        /// </remarks>
        /// <param name="semaphore">The semaphore which should be acquired.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the guard object.</returns>
        /// <exception cref="TaskCanceledException">if the operation is cancelled.</exception>
        public static async Task<SemaphoreGuard> Acquire(SemaphoreSlim semaphore, CancellationToken cancellationToken)
        {
            await semaphore.WaitAsync(cancellationToken).ConfigureAwait(false);
            
            return new SemaphoreGuard(semaphore);
        }
        
        /// <summary>
        ///     Releases the semaphore if it has been acquired.
        /// </summary>
        public void Dispose()
        {
            this.acquiredSemaphore.Release();
        }

        /// <summary>
        ///     Creates a new guard object.
        /// </summary>
        /// <remarks>
        ///     The semaphore must have been acquired prior to calling this function.
        /// </remarks>
        /// <param name="acquiredSemaphore"></param>
        private SemaphoreGuard(SemaphoreSlim acquiredSemaphore)
        {
            this.acquiredSemaphore = acquiredSemaphore;
        }
        
        /// <summary>
        ///     The successfully acquired semaphore.
        /// </summary>
        private readonly SemaphoreSlim acquiredSemaphore;
    }
}