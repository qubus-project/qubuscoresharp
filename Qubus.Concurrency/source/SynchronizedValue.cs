using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Qubus.Concurrency
{
    /// <summary>
    ///     A synchronization construct which protects a single object against concurrent accesses.
    /// </summary>
    /// <remarks>
    ///     This class is thread-safe.
    /// </remarks>
    /// <typeparam name="T">The type of the object.</typeparam>
    public sealed class SynchronizedValue<T>
    {
        /// <summary>
        ///     A guard object which manages the exclusive access to the internal state of a <see cref="SynchronizedValue{T}"/>.
        /// </summary>
        public readonly struct UpdateGuard : IDisposable
        {
            /// <summary>
            ///     Acquire the exclusive access to the <see cref="SynchronizedValue{T}"/>.
            /// </summary>
            /// <param name="value">The value to which the access should be acquired.</param>
            /// <param name="cancellationToken">The cancellation token for this operation.</param>
            /// <returns>the guard object.</returns>
            /// <exception cref="TaskCanceledException">if the operation is cancelled.</exception>
            public static async Task<UpdateGuard> Acquire(SynchronizedValue<T> value, CancellationToken cancellationToken)
            {
                // Acquire mutual exclusion.
                await value.accessSemaphore.WaitAsync(cancellationToken);
                
                // Acquire all previous stores.
                // lock/Monitor can not be used since the acquiring/release needs to happen
                // on the same managed thread which can not be guaranteed in asynchronous code.
                Interlocked.MemoryBarrier();
                
                // Create the guard object.
                return new UpdateGuard(value);
            }
            
            /// <summary>
            ///     Releases the guard.
            /// </summary>
            public void Dispose()
            {
                // Publish all stores.
                Interlocked.MemoryBarrier();
                    
                // Release the access to the protected value.
                this.value.accessSemaphore.Release();
            }

            /// <summary>
            ///     The protected value.
            /// </summary>
            public ref T Value => ref this.value.value;

            /// <summary>
            ///     Creates a new update guard.
            /// </summary>
            /// <remarks>
            ///     The access to the value must have been acquired prior to calling this function.
            /// </remarks>
            /// <param name="value">The guarded value.</param>
            private UpdateGuard(SynchronizedValue<T> value)
            {
                this.value = value;
            }

            /// <summary>
            ///     The guarded value.
            /// </summary>
            private readonly SynchronizedValue<T> value;
        }
        
        /// <summary>
        ///     Creates a new synchronized value.
        /// </summary>
        /// <param name="value">The initial value.</param>
        public SynchronizedValue(T value)
        : this(new SemaphoreSlim(1, 1), value)
        {
        }

        /// <summary>
        ///     Acquire the exclusive access to the protected object.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <returns>the guard object protecting the exclusive access.</returns>
        /// <exception cref="TaskCanceledException">if the operation is cancelled.</exception>
        public Task<UpdateGuard> Acquire(CancellationToken cancellationToken)
        {
            return UpdateGuard.Acquire(this, cancellationToken);
        }

        /// <summary>
        ///     Creates a new synchronized value which inherits the protection of this value.
        /// </summary>
        /// <remarks>
        ///     The primary use case of this method are synchronized values for subobjects.
        ///     To maintain a consistent state across the parent object, subobjects might
        ///     need to share its protection.
        /// </remarks>
        /// <param name="value">The initial value of the new synchronized value</param>
        /// <typeparam name="U">The type of the synchronized value's state.</typeparam>
        /// <returns>the newly created synchronized value.</returns>
        public SynchronizedValue<U> Inherit<U>(U value)
        {
            return new SynchronizedValue<U>(this.accessSemaphore, value);
        }

        /// <summary>
        ///     Applies the action to the internal state in a thread-safe fashion.
        /// </summary>
        /// <param name="func">The applied function.</param>
        /// <param name="cancellationToken">The cancellation token for this operation.</param>
        /// <exception cref="TaskCanceledException">if the operation is cancelled.</exception>
        public async Task Apply(Action<T> func, CancellationToken cancellationToken)
        {
            // Acquire the semaphore to ensure mutual exclusion.
            using var guard = await SemaphoreGuard.Acquire(this.accessSemaphore, cancellationToken);
            
            // Acquire all previous stores.
            // lock/Monitor can not be used since the acquiring/release needs to happen
            // on the same managed thread which can not be guaranteed in asynchronous code.
            Interlocked.MemoryBarrier();
            
            // Apply the function.
            func(this.value);
            
            // Create the guard object.
            Interlocked.MemoryBarrier();
        }

        /// <summary>
        ///     Creates a new synchronized value which inherits an already existing semaphore.
        /// </summary>
        /// <param name="accessSemaphore">The inherited semaphore.</param>
        /// <param name="value">The initial value.</param>
        private SynchronizedValue(SemaphoreSlim accessSemaphore, T value)
        {
            this.accessSemaphore = accessSemaphore;
            this.value = value;
        }
        
        /// <summary>
        ///     A binary semaphore which protects the access to the actual value.
        /// </summary>
        private readonly SemaphoreSlim accessSemaphore;

        /// <summary>
        ///     The actual value.
        /// </summary>
        private T value;
    }
}