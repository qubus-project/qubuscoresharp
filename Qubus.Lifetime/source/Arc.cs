﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Qubus.Errors;

namespace Qubus.Lifetime
{
    /// <summary>
    ///     An atomic reference counter which can be shared between different objects.
    /// </summary>
    internal sealed class SharedReferenceCounter
    {
        /// <summary>
        ///     Creates a new reference counter.
        /// </summary>
        /// <param name="initialValue">The initial value of the counter.</param>
        public SharedReferenceCounter(long initialValue)
        {
            this.refCount = initialValue;
        }

        /// <summary>
        ///     Increments the counter atomically.
        /// </summary>
        /// <returns>the new reference count.</returns>
        public long Increment()
        {
            return Interlocked.Increment(ref this.refCount);
        }

        /// <summary>
        ///     Decrements the counter atomically.
        /// </summary>
        /// <returns>the new reference count.</returns>
        public long Decrement()
        {
            return Interlocked.Decrement(ref this.refCount);
        }

        /// <summary>
        ///     Increments the counter atomically iff its value is larger than zero.
        /// </summary>
        /// <remarks>
        ///     The operation is aborted if the reference counter drops to zero before it could be incremented.
        /// </remarks>
        /// <returns>true if the counter has been incremented, false otherwise.</returns>
        public bool ConditionallyIncrement()
        {
            // Use the standard compare exchange loop to increment the value as long as the reference count never drops to zero.
            // The attempt is aborted if the reference counter drops to zero.
            for (;;)
            {
                long currentCount = Interlocked.Read(ref this.refCount);
                
                if (currentCount == 0)
                {
                    return false;
                }

                if (Interlocked.CompareExchange(ref this.refCount, currentCount + 1, currentCount) == currentCount)
                {
                    return true;
                }
            }
        }
        
        /// <summary>
        ///     The reference count.
        /// </summary>
        private long refCount;
    }
    
    /// <summary>
    ///     An automatically reference counted lifetime management object aka an arc.
    /// </summary>
    /// <remarks>
    ///     The managed resource will be discarded as soon as the reference count drops to zero.
    /// </remarks>
    /// <typeparam name="T">The type of the resource.</typeparam>
    public sealed class Arc<T> : IDisposable where T : class, IDisposable
    {
        /// <summary>
        ///     Creates a new arc.
        /// </summary>
        /// <remarks>
        ///     The lifetime of the resource must not be already managed by any other means.
        /// </remarks>
        /// <param name="value">The managed resource</param>
        public Arc(T value)
        : this(value, new SharedReferenceCounter(1))
        {
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (this.refCount == null)
            {
                return;
            }
            
            long newRefCount = this.refCount.Decrement();

            Release.Assert(newRefCount >= 0, "Invalid reference count. Too many calls to Dispose have been occured.");
            
            if (newRefCount == 0)
            {
                this.value.Dispose();
            }
        }

        /// <summary>
        ///     The managed resource.
        /// </summary>
        public T Value
        {
            get
            {
                Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");
                
                return this.value;
            }
        }

        /// <summary>
        ///     Acquires a new arc instance to keep the resource alive.
        /// </summary>
        /// <returns>the new instance.</returns>
        public Arc<T> Acquire()
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");
            
            refCount.Increment();
            
            return new Arc<T>(this.value, this.refCount);
        }

        /// <summary>
        ///     Moves the ownership of the resource to a new arc instance.
        /// </summary>
        /// <remarks>
        ///     Usually this function is used as part of the following pattern:
        ///
        ///     <code>
        ///     using Arc<MyResource> existingInstance = initResource();
        ///
        ///     \/\/ Pass the resource to another context if no error occured. Dispose it otherwise.
        ///     return existingInstance.Move();
        ///     </code>
        /// </remarks>
        /// <returns>the new instance.</returns>
        public Arc<T> Move()
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");

            var newInstance = new Arc<T>(this.value, this.refCount);

            this.refCount = null;
            
            return newInstance;
        }
        
        /// <summary>
        ///     Assumes the ownership of a resource from another arc.
        /// </summary>
        /// <remarks>
        ///     The ownership of the previously managed resource will be discarded.
        /// </remarks>
        /// <param name="other">The other arc.</param>
        /// <typeparam name="U">The resource type of the other arc.</typeparam>
        public void AssumeOwnershipFrom<U>(Arc<U> other) where U : class, T
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");

            // Release the previously managed resource.
            this.Dispose();
            
            // Replace our reference with the ones from the other instance.
            this.value = other.value;
            this.refCount = other.refCount;
            
            // Set the other instance to its empty state.
            other.refCount = null;
        }
        
        /// <summary>
        ///     A weak reference to the resource managed by an arc.
        /// </summary>
        /// <remarks>
        ///     The weak reference might be upgraded to a full arc by locking it in place as long as the resource is still referenced by at least
        ///     another arc instance.
        /// </remarks>
        public sealed class WeakReference
        {
            /// <summary>
            ///     Creates a new weak reference from an existing arc.
            /// </summary>
            /// <remarks>
            ///     The arc must still reference a resource until the constructor has returned.
            /// </remarks>
            /// <param name="arc">The arc managing the referenced resource.</param>
            public WeakReference(Arc<T> arc)
            {
                Release.Assert(arc.refCount != null, "The referenced arc is in a invalid state.");

                this.value = arc.value;
                this.refCount = arc.refCount;
            }

            /// <summary>
            ///     Locks the referenced resource in place by creating a new arc instance.
            /// </summary>
            /// <remarks>
            ///     A calls to this function are atomic. If the resource is in the process of being destroyed,
            ///     the call will not extend the lifetime of the resource, signaling this post-condition violation
            ///     via an exception.
            /// </remarks>
            /// <returns>an arc instance referencing the same resource.</returns>
            /// <exception cref="LifetimeException">if the referenced resource could not be locked.</exception>
            public Arc<T> Lock()
            {
                // Conditionally increment the reference counter.
                //
                // We do not increment the counter unconditionally since several weak references might
                // try to lock the resource at the same time as the last arc instance is being disposed.
                // If the last arc instance has already decreased the reference count to zero, none of the
                // weak references must try to keep the resource alive since the last arc instance will destory
                // it in any case.
                if (!this.refCount.ConditionallyIncrement())
                {
                    throw new LifetimeException("Unable to lock the weak reference. ");
                }

                try
                {
                    return new Arc<T>(this.value, this.refCount);
                }
                catch (System.Exception e)
                {
                    this.refCount.Decrement();

                    throw new LifetimeException("Unable to lock the weak reference. The arc instance could not be created.", e);
                }
            }

            /// <summary>
            ///     The managed resource.
            /// </summary>
            private readonly T value;
            
            /// <summary>
            ///     The reference counter which is shared with all arc instances referencing the same resource.
            /// </summary>
            private readonly SharedReferenceCounter refCount;
        }

        /// <summary>
        ///     Creates a new arc instance using a shared reference counter.
        /// </summary>
        /// <param name="value">The managed resource.</param>
        /// <param name="refCount">The shared reference counter.</param>
        private Arc(T value, SharedReferenceCounter refCount)
        {
            this.value = value;
            this.refCount = refCount;
        }

        /// <summary>
        ///     The managed resource.
        /// </summary>
        private T value;
        
        /// <summary>
        ///     The reference counter which is shared by all arc instances for a specific resource.
        /// </summary>
        private SharedReferenceCounter? refCount;
    }
    
    /// <summary>
    ///     A variant of an arc <see cref="Arc{T}"/> for asynchronous resources.
    /// </summary>
    /// <remarks>
    ///     The cleanup of the managed resource will be started as soon as the reference count drops to zero.
    /// </remarks>
    /// <typeparam name="T">The type of the resource.</typeparam>
    public sealed class AsyncArc<T> : IAsyncDisposable where T : class, IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new arc.
        /// </summary>
        /// <remarks>
        ///     The lifetime of the resource must not be already managed by any other means.
        /// </remarks>
        /// <param name="value">The managed resource</param>
        public AsyncArc(T value)
        : this(value, new SharedReferenceCounter(1))
        {
        }

        /// <inheritdoc />
        public async ValueTask DisposeAsync()
        {
            if (this.refCount == null)
            {
                return;
            }
            
            long newRefCount = this.refCount.Decrement();

            Release.Assert(newRefCount >= 0, "Invalid reference count. Too many calls to Dispose have been occured.");
            
            if (newRefCount == 0)
            {
                await this.value.DisposeAsync();
            }
        }

        /// <summary>
        ///     The managed resource.
        /// </summary>
        public T Value
        {
            get
            {
                Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");
                
                return this.value;
            }
        }

        /// <summary>
        ///     Acquires a new arc instance to keep the resource alive.
        /// </summary>
        /// <returns>The new instance.</returns>
        public AsyncArc<T> Acquire()
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");
            
            refCount.Increment();
            
            return new AsyncArc<T>(this.value, this.refCount);
        }

        /// <summary>
        ///     Moves the ownership of the resource to a new arc instance.
        /// </summary>
        /// <remarks>
        ///     Usually this function is used as part of the following pattern:
        ///
        ///     <code>
        ///     using Arc<MyResource> existingInstance = initResource();
        ///
        ///     \/\/ Pass the resource to another context if no error occured. Dispose it otherwise.
        ///     return existingInstance.Move();
        ///     </code>
        /// </remarks>
        /// <returns>The new instance.</returns>
        public AsyncArc<T> Move()
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");

            var newInstance = new AsyncArc<T>(this.value, this.refCount);

            this.refCount = null;
            
            return newInstance;
        }

        /// <summary>
        ///     Assumes the ownership of a resource from another arc.
        /// </summary>
        /// <remarks>
        ///     The ownership of the previously managed resource will be discarded.
        /// </remarks>
        /// <param name="other">The other arc.</param>
        /// <typeparam name="U">The resource type of the other arc.</typeparam>
        public async ValueTask AssumeOwnershipFrom<U>(AsyncArc<U> other) where U : class, T
        {
            Release.Assert(this.refCount != null, "This arc instance has already been disposed and does not own its resource anymore.");

            // Release the previously managed resource.
            await this.DisposeAsync();
            
            // Replace our reference with the ones from the other instance.
            this.value = other.value;
            this.refCount = other.refCount;
            
            // Set the other instance to its empty state.
            other.refCount = null;
        }

        /// <summary>
        ///     A weak reference to the resource managed by an async arc.
        /// </summary>
        /// <remarks>
        ///     The weak reference might be upgraded to a full arc by locking it in place as long as the resource is still referenced by at least
        ///     another arc instance.
        /// </remarks>
        public sealed class WeakReference
        {
            /// <summary>
            ///     Creates a new weak reference from an existing arc.
            /// </summary>
            /// <remarks>
            ///     The arc must still reference a resource until the constructor has returned.
            /// </remarks>
            /// <param name="arc">The arc managing the referenced resource.</param>
            public WeakReference(AsyncArc<T> arc)
            {
                Release.Assert(arc.refCount != null, "The referenced arc is in a invalid state.");

                this.value = arc.value;
                this.refCount = arc.refCount;
            }

            /// <summary>
            ///     Locks the referenced resource in place by creating a new arc instance.
            /// </summary>
            /// <remarks>
            ///     A calls to this function are atomic. If the resource is in the process of being destroyed,
            ///     the call will not extend the lifetime of the resource, signaling this post-condition violation
            ///     via an exception.
            /// </remarks>
            /// <returns>an arc instance referencing the same resource.</returns>
            /// <exception cref="LifetimeException">if the referenced resource could not be locked.</exception>
            public AsyncArc<T> Lock()
            {
                // Conditionally increment the reference counter.
                //
                // We do not increment the counter unconditionally since several weak references might
                // try to lock the resource at the same time as the last arc instance is being disposed.
                // If the last arc instance has already decreased the reference count to zero, none of the
                // weak references must try to keep the resource alive since the last arc instance will destory
                // it in any case.
                if (!this.refCount.ConditionallyIncrement())
                {
                    throw new LifetimeException("Unable to lock the weak reference. ");
                }

                try
                {
                    return new AsyncArc<T>(this.value, this.refCount);
                }
                catch (System.Exception e)
                {
                    this.refCount.Decrement();

                    throw new LifetimeException("Unable to lock the weak reference. The arc instance could not be created.", e);
                }
            }

            /// <summary>
            ///     The managed resource.
            /// </summary>
            private readonly T value;
            
            /// <summary>
            ///     The reference counter which is shared with all arc instances referencing the same resource.
            /// </summary>
            private readonly SharedReferenceCounter refCount;
        }

        /// <summary>
        ///     Creates a new arc instance using a shared reference counter.
        /// </summary>
        /// <param name="value">The managed resource.</param>
        /// <param name="refCount">The shared reference counter.</param>
        private AsyncArc(T value, SharedReferenceCounter refCount)
        {
            this.value = value;
            this.refCount = refCount;
        }

        /// <summary>
        ///     The managed resource.
        /// </summary>
        private T value;
        
        /// <summary>
        ///     The reference counter which is shared by all arc instances for a specific resource.
        /// </summary>
        private SharedReferenceCounter? refCount;
    }
}