﻿using System;
using System.Threading.Tasks;
using Qubus.Errors;

namespace Qubus.Lifetime
{
    /// <summary>
    ///     An exclusive owner of a resource.
    /// </summary>
    /// <typeparam name="T">The type of the resource.</typeparam>
    public sealed class Owner<T> : IDisposable where T : class, IDisposable
    {
        /// <summary>
        ///     Creates a new resource owner.
        /// </summary>
        /// <remarks>
        ///     The lifetime of the resource must not be already managed by any other means.
        /// </remarks>
        /// <param name="value">The managed resource.</param>
        public Owner(T value)
        {
            this.value = value;
        }
        
        /// <inheritdoc />
        public void Dispose()
        {
            if (this.value == null)
            {
                return;
            }
            
            this.value.Dispose();
            
            this.value = null;
        }

        /// <summary>
        ///     The managed resource.
        /// </summary>
        public T Value
        {
            get
            {
                Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");
            
                return this.value;
            }
        }

        /// <summary>
        ///     Shares the managed resource by transferring it into an arc.
        /// </summary>
        /// <remarks>
        ///     After the call, the owner has discarded the ownership of the previously managed resource.
        /// </remarks>
        /// <returns>the arc for the now shared resource.</returns>
        public Arc<T> Share()
        {
            Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");
            
            var arc = new Arc<T>(this.value);

            this.value = null;
            
            return arc;
        }
        
        /// <summary>
        ///     Moves the ownership of the resource to another owner.
        /// </summary>
        /// <remarks>
        ///     Usually this function is used as part of the following pattern:
        ///
        ///     <code>
        ///     using Owner<MyResource> existingInstance = initResource();
        ///
        ///     \/\/ Pass the resource to another context if no error occured. Dispose it otherwise.
        ///     return existingInstance.Move();
        ///     </code>
        /// </remarks>
        /// <returns>the new instance.</returns>
        public Owner<T> Move()
        {
            Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");

            var newResource = new Owner<T>(this.value);
            
            this.value = null;

            return newResource;
        }

        /// <summary>
        ///     Assumes the ownership of a resource from another owner.
        /// </summary>
        /// <remarks>
        ///     The ownership of the previously managed resource will be discarded.
        /// </remarks>
        /// <param name="other">The other arc.</param>
        /// <typeparam name="U">The resource type of the other arc.</typeparam>
        public void AssumeOwnershipFrom<U>(Owner<U> other) where U : class, T
        {
            // Release the previously managed resource.
            this.Dispose();
            
            // Replace our reference with the ones from the other instance.
            this.value = other.value;
            
            // Set the other instance to its empty state.
            other.value = null;
        }

        /// <summary>
        ///     The managed resource, if any.
        /// </summary>
        private T? value;
    }
    
    /// <summary>
    ///     A variant of an exclusive resource owner <see cref="Owner{T}"/> for asynchronous resources.
    /// </summary>
    /// <typeparam name="T">The type of the resource.</typeparam>
    public sealed class AsyncOwner<T> : IAsyncDisposable where T : class, IAsyncDisposable
    {
        /// <summary>
        ///     Creates a new resource owner.
        /// </summary>
        /// <remarks>
        ///     The lifetime of the resource must not be already managed by any other means.
        /// </remarks>
        /// <param name="value">The managed resource.</param>
        public AsyncOwner(T value)
        {
            this.value = value;
        }
        
        /// <inheritdoc />
        public async ValueTask DisposeAsync()
        {
            if (this.value == null)
            {
                return;
            }
            
            await this.value.DisposeAsync();
            
            this.value = null;
        }
        
        /// <summary>
        ///     The managed resource.
        /// </summary>
        public T Value
        {
            get
            {
                Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");
            
                return this.value;
            }
        }
        
        /// <summary>
        ///     Shares the managed resource by transferring it into an arc.
        /// </summary>
        /// <remarks>
        ///     After the call, the owner has discarded the ownership of the previously managed resource.
        /// </remarks>
        /// <returns>the arc for the now shared resource.</returns>
        public AsyncArc<T> Share()
        {
            Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");
            
            var arc = new AsyncArc<T>(this.value);

            this.value = null;
            
            return arc;
        }

        /// <summary>
        ///     Moves the ownership of the resource to another owner.
        /// </summary>
        /// <remarks>
        ///     Usually this function is used as part of the following pattern:
        ///
        ///     <code>
        ///     await using AsyncOwner<MyResource> existingInstance = initResource();
        ///
        ///     \/\/ Pass the resource to another context if no error occured. Dispose it otherwise.
        ///     return existingInstance.Move();
        ///     </code>
        /// </remarks>
        /// <returns>the new instance.</returns>
        public AsyncOwner<T> Move()
        {
            Release.Assert(this.value != null, "This lifetime helper has already been disposed and does not own its resource anymore.");

            var newResource = new AsyncOwner<T>(this.value);
            
            this.value = null;

            return newResource;
        }
        
        /// <summary>
        ///     Assumes the ownership of a resource from another owner.
        /// </summary>
        /// <remarks>
        ///     The ownership of the previously managed resource will be discarded.
        /// </remarks>
        /// <param name="other">The other arc.</param>
        /// <typeparam name="U">The resource type of the other arc.</typeparam>
        public async ValueTask AssumeOwnershipFrom<U>(AsyncOwner<U> other) where U : class, T
        {
            // Release the previously managed resource.
            await this.DisposeAsync();
            
            // Replace our reference with the ones from the other instance.
            this.value = other.value;
            
            // Set the other instance to its empty state.
            other.value = null;
        }

        /// <summary>
        ///     The managed resource, if any.
        /// </summary>
        private T? value;
    }
}