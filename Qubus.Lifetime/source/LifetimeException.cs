﻿using Qubus.Errors;

namespace Qubus.Lifetime
{
    /// <summary>
    ///     An exception thrown by a component of the lifetime module.
    /// </summary>
    public class LifetimeException : Exception
    {
        /// <summary>
        ///     Creates a default exception.
        /// </summary>
        public LifetimeException()
        {
        }
        
        /// <summary>
        ///     Creates a new exception from a message describing the cause of the error.
        /// </summary>
        /// <param name="message">The message describing the error.</param>
        public LifetimeException(string message)
        : base(message)
        {
        }
        
        /// <summary>
        ///     Creates a new exception from a message and an inner exception describing the cause of the error.
        /// </summary>
        /// <param name="message">The message describing the error.</param>
        /// <param name="innerException">The exception causing the post-condition violation leading to this exception.</param>
        public LifetimeException(string message, System.Exception innerException)
        : base(message, innerException)
        {
        }
    }
}