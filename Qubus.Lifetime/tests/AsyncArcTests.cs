﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using Qubus.Lifetime;
using Moq;

namespace Qubus.LifetimeTests
{
    public class AsyncArcTests
    {
        [Test]
        public async Task TestValue()
        {
            var disposable = new Mock<IAsyncDisposable>();

            await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);

            Assert.AreEqual(disposable.Object, instance.Value);
        }

        [Test]
        public async Task TestAcquire()
        {
            var disposable = new Mock<IAsyncDisposable>();

            AsyncArc<IAsyncDisposable> otherInstance;

            {
                await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);

                otherInstance = instance.Acquire();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);

            await otherInstance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        
        [Test]
        public async Task TestMove()
        {
            var disposable = new Mock<IAsyncDisposable>();

            AsyncArc<IAsyncDisposable> otherInstance;

            {
                await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);

                otherInstance = instance.Move();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);
            
            await otherInstance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        
        [Test]
        public async Task TestAssumeOwnershipFrom()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var disposableDummy = new Mock<IAsyncDisposable>();
            
            var otherInstance = new AsyncArc<IAsyncDisposable>(disposableDummy.Object);

            {
                await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);

                await otherInstance.AssumeOwnershipFrom(instance);
                
                disposableDummy.Verify(disposableDummy => disposableDummy.DisposeAsync(), Times.Once);
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);
            
            await otherInstance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        
        [Test]
        public async Task TestDispose()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);
            
            await instance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        

        [Test]
        public async Task TestWeakReferenceLock_TheArcIsStillAlive_ANewArcInstanceIsReturned()
        {
            var disposable = new Mock<IAsyncDisposable>();

            await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);
            
            var weakRef = new AsyncArc<IAsyncDisposable>.WeakReference(instance);

            await using AsyncArc<IAsyncDisposable> instance2 = weakRef.Lock();
            
            // Both instances point to the same resource.
            Assert.AreSame(instance.Value, instance2.Value);
            
            // The resource is still alive.
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);
        }
        
        [Test]
        public async Task TestWeakReferenceLock_TheLastArcHasBeenDisposed_TheResourceIsDestoryedAndAnExceptionIsThrown()
        {
            var disposable = new Mock<IAsyncDisposable>();

            AsyncArc<IAsyncDisposable>.WeakReference weakRef;

            {
                await using var instance = new AsyncArc<IAsyncDisposable>(disposable.Object);

                weakRef = new AsyncArc<IAsyncDisposable>.WeakReference(instance);
            }
            
            // The resource is has been destroyed even though a weak reference to it exists.
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);

            // The weak reference can not be locked after the resource has been disposed.
            Assert.Throws<LifetimeException>(() => weakRef.Lock());
        }
        
        [Test]
        public async Task TestAutoDisposePattern()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var consumer = new Mock<Action<IAsyncDisposable>>();

            await using var arc = new AsyncArc<IAsyncDisposable>(disposable.Object);
            
            consumer.Object.Invoke(arc.Move());
        }
        
        [Test]
        public async Task TestAutoDisposePatternWithIteration()
        {
            var currentDisposable = new Mock<IAsyncDisposable>();

            await using var currentResource = new AsyncArc<IAsyncDisposable>(currentDisposable.Object);
            
            Assert.AreSame(currentDisposable.Object, currentResource.Value);

            for (int i = 0; i < 10; ++i)
            {
                var newDisposable = new Mock<IAsyncDisposable>();
                
                await using var newResource = new AsyncArc<IAsyncDisposable>(newDisposable.Object);

                await currentResource.AssumeOwnershipFrom(newResource);

                currentDisposable.Verify(currentDisposable => currentDisposable.DisposeAsync(), Times.Once);
                
                currentDisposable = newDisposable;
                
                Assert.AreSame(newDisposable.Object, currentResource.Value);
            }
        }
    }
}