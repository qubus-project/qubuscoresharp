﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using Qubus.Lifetime;
using Moq;

namespace Qubus.LifetimeTests
{
    public class AsyncOwnerTests
    {
        [Test]
        public async Task TestValue()
        {
            var disposable = new Mock<IAsyncDisposable>();

            await using var instance = new AsyncOwner<IAsyncDisposable>(disposable.Object);

            Assert.AreEqual(disposable.Object, instance.Value);
        }

        [Test]
        public async Task TestMove()
        {
            var disposable = new Mock<IAsyncDisposable>();

            AsyncOwner<IAsyncDisposable> otherInstance;

            {
                await using var instance = new AsyncOwner<IAsyncDisposable>(disposable.Object);

                otherInstance = instance.Move();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);
            
            await otherInstance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        
        [Test]
        public async Task TestAssumeOwnershipFrom()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var disposableDummy = new Mock<IAsyncDisposable>();
            
            var otherInstance = new AsyncOwner<IAsyncDisposable>(disposableDummy.Object);

            {
                await using var instance = new AsyncOwner<IAsyncDisposable>(disposable.Object);

                await otherInstance.AssumeOwnershipFrom(instance);
                
                disposableDummy.Verify(disposableDummy => disposableDummy.DisposeAsync(), Times.Once);
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);
            
            await otherInstance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
        
        [Test]
        public async Task TestDispose()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var instance = new AsyncOwner<IAsyncDisposable>(disposable.Object);
            
            await instance.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }

        [Test]
        public async Task TestShare()
        {
            var disposable = new Mock<IAsyncDisposable>();

            var instance = new AsyncOwner<IAsyncDisposable>(disposable.Object);

            AsyncArc<IAsyncDisposable> arc = instance.Share();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Never);

            await arc.DisposeAsync();
            
            disposable.Verify(disposable => disposable.DisposeAsync(), Times.Once);
        }
    }
}