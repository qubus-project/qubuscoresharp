using System;
using NUnit.Framework;
using Qubus.Lifetime;
using Moq;

namespace Qubus.LifetimeTests
{
    public class ArcTests
    {
        [Test]
        public void TestValue()
        {
            var disposable = new Mock<IDisposable>();

            using var instance = new Arc<IDisposable>(disposable.Object);

            Assert.AreEqual(disposable.Object, instance.Value);
        }

        [Test]
        public void TestAcquire()
        {
            var disposable = new Mock<IDisposable>();

            Arc<IDisposable> otherInstance;

            {
                using var instance = new Arc<IDisposable>(disposable.Object);

                otherInstance = instance.Acquire();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.Dispose(), Times.Never);

            otherInstance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestMove()
        {
            var disposable = new Mock<IDisposable>();

            Arc<IDisposable> otherInstance;

            {
                using var instance = new Arc<IDisposable>(disposable.Object);

                otherInstance = instance.Move();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.Dispose(), Times.Never);
            
            otherInstance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestAssumeOwnershipFrom()
        {
            var disposable = new Mock<IDisposable>();

            var disposableDummy = new Mock<IDisposable>();
            
            var otherInstance = new Arc<IDisposable>(disposableDummy.Object);

            {
                using var instance = new Arc<IDisposable>(disposable.Object);

                otherInstance.AssumeOwnershipFrom(instance);
                
                disposableDummy.Verify(disposableDummy => disposableDummy.Dispose(), Times.Once);
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.Dispose(), Times.Never);
            
            otherInstance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestDispose()
        {
            var disposable = new Mock<IDisposable>();

            var instance = new Arc<IDisposable>(disposable.Object);
            
            instance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }

        [Test]
        public void TestWeakReferenceLock_TheArcIsStillAlive_ANewArcInstanceIsReturned()
        {
            var disposable = new Mock<IDisposable>();

            using var instance = new Arc<IDisposable>(disposable.Object);
            
            var weakRef = new Arc<IDisposable>.WeakReference(instance);

            using Arc<IDisposable> instance2 = weakRef.Lock();
            
            // Both instances point to the same resource.
            Assert.AreSame(instance.Value, instance2.Value);
            
            // The resource is still alive.
            disposable.Verify(disposable => disposable.Dispose(), Times.Never);
        }
        
        [Test]
        public void TestWeakReferenceLock_TheLastArcHasBeenDisposed_TheResourceIsDestoryedAndAnExceptionIsThrown()
        {
            var disposable = new Mock<IDisposable>();

            Arc<IDisposable>.WeakReference weakRef;

            {
                using var instance = new Arc<IDisposable>(disposable.Object);

                weakRef = new Arc<IDisposable>.WeakReference(instance);
            }

            // The resource is has been destroyed even though a weak reference to it exists.
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);

            // The weak reference can not be locked after the resource has been disposed.
            Assert.Throws<LifetimeException>(() => weakRef.Lock());
        }

        [Test]
        public void TestAutoDisposePattern()
        {
            var disposable = new Mock<IDisposable>();

            var consumer = new Mock<Action<IDisposable>>();

            using var arc = new Arc<IDisposable>(disposable.Object);
            
            consumer.Object.Invoke(arc.Move());
        }
        
        [Test]
        public void TestAutoDisposePatternWithIteration()
        {
            var currentDisposable = new Mock<IDisposable>();

            using var currentResource = new Arc<IDisposable>(currentDisposable.Object);
            
            Assert.AreSame(currentDisposable.Object, currentResource.Value);

            for (int i = 0; i < 10; ++i)
            {
                var newDisposable = new Mock<IDisposable>();
                
                using var newResource = new Arc<IDisposable>(newDisposable.Object);

                currentResource.AssumeOwnershipFrom(newResource);
                
                currentDisposable.Verify(currentDisposable => currentDisposable.Dispose(), Times.Once);
                
                currentDisposable = newDisposable;

                Assert.AreSame(newDisposable.Object, currentResource.Value);
            }
        }
    }
}