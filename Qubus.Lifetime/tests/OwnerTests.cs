﻿using System;
using NUnit.Framework;
using Qubus.Lifetime;
using Moq;

namespace Qubus.LifetimeTests
{
    public class OwnerTests
    {
        [Test]
        public void TestValue()
        {
            var disposable = new Mock<IDisposable>();

            using var instance = new Owner<IDisposable>(disposable.Object);

            Assert.AreEqual(disposable.Object, instance.Value);
        }

        [Test]
        public void TestMove()
        {
            var disposable = new Mock<IDisposable>();

            Owner<IDisposable> otherInstance;

            {
                using var instance = new Owner<IDisposable>(disposable.Object);

                otherInstance = instance.Move();
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.Dispose(), Times.Never);
            
            otherInstance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestAssumeOwnershipFrom()
        {
            var disposable = new Mock<IDisposable>();

            var disposableDummy = new Mock<IDisposable>();
            
            var otherInstance = new Owner<IDisposable>(disposableDummy.Object);

            {
                using var instance = new Owner<IDisposable>(disposable.Object);

                otherInstance.AssumeOwnershipFrom(instance);
                
                disposableDummy.Verify(disposableDummy => disposableDummy.Dispose(), Times.Once);
            }
            
            Assert.AreEqual(disposable.Object, otherInstance.Value);

            disposable.Verify(disposable => disposable.Dispose(), Times.Never);
            
            otherInstance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestDispose()
        {
            var disposable = new Mock<IDisposable>();

            var instance = new Owner<IDisposable>(disposable.Object);
            
            instance.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
        
        [Test]
        public void TestShare()
        {
            var disposable = new Mock<IDisposable>();

            var instance = new Owner<IDisposable>(disposable.Object);

            Arc<IDisposable> arc = instance.Share();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Never);

            arc.Dispose();
            
            disposable.Verify(disposable => disposable.Dispose(), Times.Once);
        }
    }
}