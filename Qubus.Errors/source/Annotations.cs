using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace Qubus.Errors
{
    /// <summary>
    ///     A set of code annotations which are also checked in release mode.
    /// </summary>
    public static class Release
    {
        /// <summary>
        ///     Asserts that the condition is true.
        /// </summary>
        /// <param name="condition">The condition.</param>
        /// <param name="message">A diagnostic message.</param>
        public static void Assert([DoesNotReturnIf(false)] bool condition, string message)
        {
            if (!condition)
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
                else
                {
                    // Kill the application ASAP if the condition is violated.
                    Environment.FailFast(message);
                }
            }
        }
        
        /// <summary>
        ///     Asserts that the location marked by a call to this function can never be reached.
        /// </summary>
        /// <param name="message">A diagnostic message.</param>
        [DoesNotReturn]
        public static void Unreachable(string message)
        {
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
            else
            {
                // Kill the application ASAP. This function should never be called.
                Environment.FailFast(message);
            }
        }
    }
}