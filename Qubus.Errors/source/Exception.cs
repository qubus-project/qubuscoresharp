﻿namespace Qubus.Errors
{
    /// <summary>
    ///     The base class for the entire Qubus exception hierarchy.
    /// </summary>
    public class Exception : System.Exception
    {
        /// <summary>
        ///     Creates a default exception.
        /// </summary>
        public Exception()
        {
        }
        
        /// <summary>
        ///     Creates a new exception from a message describing the cause of the error.
        /// </summary>
        /// <param name="message">The message describing the error.</param>
        public Exception(string message)
        : base(message)
        {
        }
        
        /// <summary>
        ///     Creates a new exception from a message and an inner exception describing the cause of the error.
        /// </summary>
        /// <param name="message">The message describing the error.</param>
        /// <param name="innerException">The exception causing the post-condition violation leading to this exception.</param>
        public Exception(string message, System.Exception innerException)
        : base(message, innerException)
        {
        }
    }
}